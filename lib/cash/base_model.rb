class ActiveRecord::Relation 
  
  # Implement 'to_hash' method in data collection
  def to_hash
    [].tap { |a| self.each { |v| a << v.to_hash  } }
  end
  
end


module Cash
  
  module Model
  end
  
  # Base model for Tango resources
  # 
  # @author Mckomo
  class BaseModel < ::ActiveRecord::Base
    
    # Required by ActiveRecord
    self.abstract_class = true
    
    # Representation of model object as hash
    #
    # @return [Hash]
    def to_hash
      raise NotImplementedError
    end
    
  end
  
end

