require_relative '../support/helper'

class ParametersTest < Test::Unit::TestCase
  
  context Cash::Parameters do
    
    should "have only keys from a white-list" do
      
      request_parameters = {
        "test_id"   => [1, 2, 3],
        "scope"     => "full",
        "tutor_id"  => [6, 9],
        "group_id"  => [3, 4]
      }

      whitelist = ["tutor_id", "group_id"]
      parameters = Cash::Parameters.new( whitelist, request_parameters )

      assert_equal ["group_id", "tutor_id"], parameters.keys 

    end

    should "have keys in alphabetic order" do

      request_parameters = {
        "z"   => [1, 2, 3],
        "a"   => [1, 2, 3],
        "c"   => [1, 2, 3],
        "b"   => [1, 2, 3]
      }

      whitelist = ["z", "a", "c", "b"]
      parameters = Cash::Parameters.new( whitelist, request_parameters )

      assert_equal parameters.keys, ['a', 'b', 'c', 'z']

    end

    should "store values in array" do
      
      request_parameters = {
        "key"   => 58
      }

      whitelist = ["key"]
      parameters = Cash::Parameters.new( whitelist, request_parameters )

      assert_equal parameters["key"], [58]

    end

    should "store values in increasing order" do

      request_parameters = {
        "key"   => [1, 68, 2, 57]
      }

      whitelist = ["key"]
      parameters = Cash::Parameters.new( whitelist, request_parameters )

      assert_equal parameters["key"], [1, 2, 57, 68]

    end

    should "only store integers" do

      request_parameters = {
        "key"   => ["12", "foo", 123]
      }

      whitelist = ["key"]
      parameters = Cash::Parameters.new( whitelist, request_parameters )

      assert_equal parameters["key"], [12, 123]

    end
  
  end
  
end