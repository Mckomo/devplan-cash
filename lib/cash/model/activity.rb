module Cash
  module Model
    
    # @author Mckomo
    class Activity < Cash::BaseModel
      
      has_many :participations
      has_many :groups, :through => :participations
      has_many :assignments
      has_many :tutors, :through => :assignments
      has_many :localizations
      has_many :places, :through => :localizations
      
      @@states = {
        0 => 'inactive',
        1 => 'active',
        2 => 'remarks',
        3 => 'postponed'
      }.freeze
      
      @@days_of_week = {
        'Nd' => 'Niedziela', 
        'Pn' => 'Poniedziałek',
        'Wt' => 'Wtorek',
        'Śr' => 'Środa',
        'Cz' => 'Czwartek',
        'Pt' => 'Piątek',
        'Sb' => 'Sobota'
        }.freeze
    
      def to_hash
        
        {
          id:                   id,
          places:               places ? places.to_hash : nil,
          tutors:               tutors ? tutors.to_hash : nil,
          groups:               groups ? groups.to_hash : nil, 
          category:             1,
          category_name:        category,
          name:                 name,
          notes:                notes || "",
          url:                  "",
          date:                 date,
          day_of_week:          day_of_week_numeric,
          day_of_week_text:     day_of_week_text || "",
          starts_at:            starts_at,
          ends_at:              ends_at,
          regular_schedule:     1, 
          starts_at_timestamp:  starts_at_timestamp,
          ends_at_timestamp:    ends_at_timestamp,
          canceled:             false,
          candeled_reason:      ""
        }
        
      end

      private
      
      def day_of_week_numeric
        @@days_of_week.keys.index(day_of_week)
      end
      
      def day_of_week_text
        @@days_of_week[day_of_week]
      end
      
    end
    
  end 
end
