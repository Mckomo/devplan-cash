module Cash
  module Model
    
    # @author Mckomo
    class Assignment < Cash::BaseModel
  
      belongs_to :tutor
      belongs_to :activity
      
    end
    
  end
end