module Cash
  module Model
    
    # @author Mckomo
    class Tutor < Cash::BaseModel
  
      has_many :assignments 
      has_many :activities, :through => :assignments
  
      def to_hash
        {
          id:         id,
          name:       name,
          short_name: name,
          moodle_url: moodle_url,
          regular:    true
        }
      end
      
      def moodle_url
        "https://e-uczelnia.uek.krakow.pl/course/view.php?id=#{moodle_id}" if moodle_id
      end
      
    end
    
  end
end
