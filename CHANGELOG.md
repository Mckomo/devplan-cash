#UEK-Cach changelog

## API v. 0.2

### 0.2.0 - 20/04/2014

## API v. 0.1

### 0.1
- 0.1 is alpha version of Cash web service. Still requires a lot of work, but basic functionality was achieved.

### 0.1.1
- Cash fallows changes in database data structure. Moved from timestamp to keep date and time in separate columns.

### 0.1.2
- Implemented resource versioning
- Database synchronization with Tango

### 0.1.3 - 26/02/2014
- Code refactoring
- Followed changes of Tango
- Added 'scope' parameter to /timetables

### 0.2.0 - 30/06/2014
- Follow changes of Tango database structure

### 0.2.1 - 01/11/2014
- Added iCalendar format for timetables