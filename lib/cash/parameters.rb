module Cash
  
  # @author Mckomo
  class Parameters < Hash
        
    def initialize( params_whitelist, request_params )
      
      # Set allowed params
      @params_whitelist = params_whitelist
      
      # Extract params from 
      extract( request_params )
      
    end
    
    private 
    
    # Clean out unsupported params and order it's keys and values
    #
    # @param  [Hash]
    # @return [Nil]
    def extract( params )
                
        # Iterate through each post param to filter it
        params.keys.sort.each do |key|

          value = params[key]
          
          # Skip param unless it's on whitelist
          next unless @params_whitelist.include? key
          
          # Store param values as array
          values =  Array( value ).uniq.map { |v| v.respond_to?( :to_i ) ? v.to_i : nil }.select { |v| v > 0 }   
          # If there are any values after filtration
          if values.size > 0
            # Sort them and append to the param hash
            self[key] = values.sort
          end
          
        end
        
    end
    
  end
end