module Cash
  
  module SinatraHelper
    
    # Prepare response for failed request
    # 
    # @param  [Integer]
    # @param  [String]
    # @return [Nil]
    def error( status = 404, message = "" )
      response.status = status
      response.body = { status: status, message: message }
    end
    
    # Enable AJAX requests
    #
    # @param    domain  [String]  Authorized domain for AJAX requests
    # @return           [Nil]
    def enable_cors(domain = '*')
      headers({ 
        'Access-Control-Allow-Origin' => domain, 
        'Access-Control-Allow-Methods' => 'OPTIONS',
        'Access-Control-Allow-Headers' => 'Content-Type'
      })
      halt 200 if request.options?
    end

    def parse_input
      begin
        params.merge! JSON.parse(env['rack.input'].read)
      rescue
      end
    end
    
    # Convert body of request to JSON format
    def body_to_json
     body( response.body.to_json )
    end
    
    # Return request URL without query part
    #
    # @return   [String]
    def request_url
      request.url.split( '?' ).first
    end
    
  end
  
end
