# Cash is a REST API for resources delivered by Tango

# Load Cash module files
require_relative 'cash/base_model'
require_relative 'cash/parameters'
require_relative 'cash/timetable'
require_relative 'cash/unit_counter'
require_relative 'cash/sinatra_helper'
require_relative 'cash/api'

# Load models
Dir.glob( "./lib/cash/model/*.rb" ).each { |f| require f }
