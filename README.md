# devPlan-Cash

Gabriel Cash was always "mouth" in [Tango & Cash](https://bitbucket.org/Mckomo/devplan-docs) partnership. So it is in this case. Cash delivers content through it's RESTful API.

## Environment requirements

* Ruby >= 2.1.2
* MongoDB >= 2.4.5
* Redis >= 2.8.0

## REST API v. 0.2

Version 0.2 is beta version of Cash web service. It is open and does not require any authentication. All responses are returned in **JSON** format. Check out the list of all public API calls.

### Resources:

#### Tutor

##### Properties:


| Property          | Type              | Description |
|:-----------------:|:-----------------:| ----------- |
| **id**            | Integer           | Unique persistent identifier
| **name**          | String            | Full name of a tutor with titles
| **short_name**    | String            | Name shortcut without titles 
| **moodle_url**    | String            | URL address to tutor's moodle profile, can be **empty string**
| **regular**       | Boolean           | *True* if tutor is regular university employee, otherwise *false*

##### Example object:

```
{  
	"id":266,
	"name":"prof. UEK Paweł Lula",
	"short_name":"P. Lula",
	"moodle_url":"https://e-uczelnia.uek.krakow.pl/course/view.php?id=1034",
	"regular":true
}
```

#### Place

##### Properties:


| Property          | Type              | Description |
|:-----------------:|:-----------------:| ----------- |
| **id**            | Integer           | Unique persistent identifier
| **name**          | String            | Default abbreviated form of the place's name
| **full_name**     | String            | Full name of the place without any abbreviation 
| **regular**       | Boolean           | *True* if the place is a regular university classroom, otherwise *false*.

##### Example object:

```
{  
	"id":97,
	"name":"Paw. C s. A",            
	"full_name":"Pawilon C sala A",
	"regular":true
}
```

#### Group

##### Properties:


| Property          | Type              | Description |
|:-----------------:|:-----------------:| ----------- |
| **id**            | Integer           | Unique persistent identifier
| **name**          | String            | Name of the group
| **type**          | Integer           | Type of the group as integer
| **type_name**     | String            | Name of the type from the `type` field 

##### Types of a group

| Type        | Type name |
|:-----------:|:---------:|
| 0 (default) | Grupa specjalna   
| 1           | Grupa dziekańska
| 2           | Grupa językowa
| 3           | Grupa seminaryjna

##### Example object:

```
{  
	"id":552,
	"name":"KrDZIs3011Io",
	"type":1,
	"type_name":"Grupa dziekańska"
}
```

#### Activity

##### Properties:

| Property                | Type              | Description |
|:-----------------------:|:-----------------:| ----------- |
| **id**                  | Integer           | Unique identifier, but not persistent. Can change with a timetable update
| **places**              | Array             | Array of zero, one or many places where the activity take place
| **tutors**              | Array             | Array of zero, one or many tutors that conduct the activity
| **groups**              | Array             | Array of zero, one or many groups that participate in the activity
| **category**            | Integer           | Category of the activity as integer
| **category_name**       | String            | Name of the category from the `category` field
| **name**                | String            | Name of the activity
| **regular_course**      | Boolean           | *True* if the activity is a part of a regular university course, otherwise *false*
| **notes**               | String            | Notes attached to the activity, can be **empty string**
| **url**                 | String            | URL attached to the activity, can be **empty string**
| **date**                | String            | Date of the activity in yyyy-mm-dd format
| **day_of_week**         | Integer           | Number of the day of a week from the `date` field. Can be an integer from [0, 6] range, where Sunday is 0
| **day_of_week_text**    | String            | Name of the day of a week from the `day_of_week` field
| **starts_at**           | String            | Time of the activity beginning in H:mm format
| **ends_at**             | String            | Time of the activity ending in H:mm format
| **regular_schedule**    | Boolean           | *True* if the activity start and end time match university regular timetable, otherwise *false*
| **starts_at_timestamp** | Integer           | Timestamp of the activity beginning
| **ends_at_timestamp**   | Integer           | Timestamp of the activity ending
| **unit_ordinal_number** | Integer           | Ordinal number of the activity in a course block
| **unit_total_count**    | Integer           | Total number of activities in a course block
| **canceled**            | Boolean           | *True* if the activity has been canceled, otherwise *false*
| **canceled_reason**     | String            | Reason why the activity was canceled, can be **empty string**


##### Categories of an activity 

| Category    | Type name |
|:-----------:|:---------:|
| 0 (default) | specjalne
| 1           | wykład
| 2           | ćwiczenia
| 3           | lektorat
| 4           | egzamin

##### Example object:

```
{
	"id":18282,
	"places":[ 
		// place objects
	],
	"tutors":[ 
	   // tutor objects
	],
	"groups":[ 
	   // group objets
	],
	"category":1               
	"category_name":"wykład",
	"name":"Teoria grafów",  	
	"regular_course":true,
	"notes":"",
	"url":"",
	"date":"2014-02-07",
	"day_of_week":5,
	"day_of_week_text":"Piątek",
	"starts_at":"9:35",
	"ends_at":"14:45",
	"regular_schedule":true,
	"starts_at_timestamp":1391765400,
	"ends_at_timestamp":1391771100, 
	"unit_ordinal_number":"1",
	"unit_total_count":"15", 
	"canceled":false,
	"canceled_reason":""
}
```

#### Timetable

##### Properties:


| Property                | Type              | Description |
|:-----------------------:|:-----------------:| ----------- |
| **_id**                 | String            | Unique persistent identifier
| **params**              | Dictionary        | Dictionary with parameters used in POST request
| **version**             | String            | Version of the timetable. It changes whenever one or more `activities` have changed
| **activities**          | Array             | Array of zero, one or many activities


##### Example object:

```
{  
   "_id":"g552g3328",
   "params":{ 
      "group_id":[  
         552,
         3328
      ]
   },
   "version":"5096146ca522d316c87b217b83a6c4d2",
   "activities": [
   		// activity objects
   ]
}
```

### Endpoints:

#### Get list of tutors

Returns collection of all tutors who carry out **activities**.

##### Resource URI:

```
GET /tutors
```
##### Parameters
None

##### Example request:

**GET** http://devplan.uek.krakow.pl/api/tutors

```
[
   {  
      "id":1,
      "name":"dr Marcin Kędzior",
      "short_name": "M. Kędzior",
      "moodle_url":"https://e-uczelnia.uek.krakow.pl/course/view.php?id=651",
      "regular":true
   },
   ...
   {  
      "id":156,
      "name":"dr Zbigniew Stańczyk",
      "short_name": "Z. Stańczyk",
      "moodle_url":"https://e-uczelnia.uek.krakow.pl/course/view.php?id=367",     
      "regular":true
   }
]
```

#### Get list of places

Returns collection of all places where **activities** take place.

##### Resource URI:

```
GET /places
```
##### Parameters
None

##### Example request:

**GET** http://devplan.uek.krakow.pl/api/places

```
[
   {  
      "id":1,
      "name":"Paw. C aula nowa",
      "full_name":"Pawilon C aula nowa",
      "regular":true
   },
   ...
   {  
      "id":98,
      "name":"Paw. F s. 717",
      "full_name":"Pawilon F sala 717",
      "regular":true
   }
]
```

#### Get list of groups

Returns collection of all groups that participate in **activities**.

##### Resource URI:

```
GET /groups
```
##### Parameters
None

##### Example request:

**GET** http://devplan.uek.krakow.pl/api/groups

```
[
   {  
      "id":1,
      "name":"AgH301A1",
      "type":"1",
      "type_name":"Grupa dziekańska"
   },
	...
   {  
      "id":918,
      "name":"ZoD201B1"
      "type":"1",
      "type_name":"Grupa dziekańska"
   }
]
```
#### Get registered timetable

Returns previously registered timetable containing **activities**.

##### Resource URI:

```
GET /timetables/:id/:scope
```
##### Parameters




| Name      | Type              | Description
|:---------:|:-----------------:| ---
| **id**    | String *required* | Id
| **scope** | String *optional* | Scope
 



##### Example requests:

**GET** http://devplan.uek.krakow.pl/api/timetables/g552g3328

```
{  
   "_id":"g552g3328",      
   "params":{              
      "group_id":[  
         552,
         3328
      ]
   },
   "version":"5096146ca522d316c87b217b83a6c4d2",
   "activities":[ 
      { 
         "id":18282,
         "places":[           
            {  
               "id":97,                          
               "name":"Paw. C s. A",             
               "full_name":"Pawilon C sala A",   
               "regular":true                    
            }
         ],
         "tutors":[
            {  
               "id":266,
               "name":"prof. UEK Paweł Lula", 
               "short_name":"P. Lula",
               "moodle_url":"https://e-uczelnia.uek.krakow.pl/course/view.php?id=1034",
               "regular":true
            }
         ],
         "groups":[  
            {  
               "id":552,
               "name":"KrDZIs3011Io",
               "type":1,
               "type_name":"Grupa dziekańska"
            },
            {  
               "id":553,
               "name":"KrDZIs3011Si",
               "type":1,
               "type_name":"Grupa dziekańska"
            },
            {  
               "id":554,
               "name":"KrDZIs3012Si",
               "type":1,
               "type_name":"Grupa dziekańska"
            }
         ],
   		"category":1               
   		"category_name":"wykład",
   		"name":"Teoria grafów",  	
   		"regular_course":true,
   		"notes":"",
   		"url":"",
   		"date":"2014-02-07",
   		"day_of_week":5,
   		"day_of_week_text":"Pt",
   		"starts_at":"9:35",
   		"ends_at":"14:45",
   		"regular_schedule":true,
   		"starts_at_timestamp":1391765400,
   		"ends_at_timestamp":1391771100, 
   		"unit_ordinal_number":"1",
   		"unit_total_count":"15", 
   		"canceled":false,
   		"canceled_reason":""
         },
      ...
   ]
}
```
**GET** http://devplan.uek.krakow.pl/api/timetables/g552g3328/version

```
{  
   "_id":"g552g3328",
   "version":"16377fc41997afd73423f43250530b22"
}
```

#### Register timetable

Returns previously registered timetable containing **activities**.

##### Resource URI:

```
POST /timetables
```
##### Parameters

| Name         | Type                       | Description |
|:------------:|:--------------------------:| ----------- |
| **group_id** | String or Array *optional* | Id or ids of groups
| **place_id** | String or Array *optional* | Id or ids of places
| **tutor_id** | String or Array *optional* | Id or ids of tutors

You have to pass at least one parameter.

##### Example request:

**POST** http://devplan.uek.krakow.pl/api/timetables
``` "group_id[]=552&group_id[]=3328"```

```
{  
   "_id":"g552g3328",
   "access_url":"http://devplan.uek.krakow.pl/api/timetables/g552g3328",
   "params":{  
      "group_id":[  
         552,
         3328
      ]
   }
}
```

### Example usage - registering your own timetable

Let's say you want to create your own timetable. You can achieve that in a few simple steps. To make an example lets establish that you belong to two groups: *KrDZIs3011Io* (6th semester of applied computer scinece) and *SJO-DUGaA025B2* (B2 level of English course).

**1.** Get list of all groups to find ids of yours. 

**Request**:

```
GET http://devplan.uek.krakow.pl/api/groups  	
```

**Response:**
	
```
[
	...
   {  
      "id":552,
      "name":"KrDZIs3011Io",
      "type":1,
      "type_name":"Grupa dziekańska"
   },
   {  
      "id":553,
      "name":"KrDZIs3011Si",
      "type":1,
      "type_name":"Grupa dziekańska"
   },
   {  
      "id":554,
      "name":"KrDZIs3012Si",
      "type":1,
      "type_name":"Grupa dziekańska"
}
	...	
]
```
	
**2.** Now you can see that your groups have ids 12 and 51. With that knowledge you can register your own timetable.
	
**Request**:

```
POST http://devplan.uek.krakow.pl/api/timetables "group_id[]=552&group_id[]=1403"
```
 	
**Response:**
 	
```
{  
   "_id":"g552g1403",
   "access_url":"http://devplan.uek.krakow.pl/api/timetables/g552g1403",
   "params":{  
      "group_id":[  
         552,
         1403
      ]
   }
}
```

**3a.** Now you can get your freshly created timetable.

**Request**:

```
GET http://devplan.uek.krakow.pl/api/timetables/g552g1403
```
 	
**Response:**

```
{  
   "_id":"g552g1403",
   "params":{  
      "group_id":[  
         552,
         1403
      ]
   },
   "version":"a044b9341e0c322b8fbec2db4a2cd8c2",
   "activities":[
		...
   ]
}
```

**3b.** Moreover, just to look up whether your timetable has changed, you can download light `version` scope of timetable.

**Request**

```
http://devplan.uek.krakow.pl/api/timetables/g552g1403/version
```

**Response:**

```
{  
   "_id":"g552g1403",
   "version":"a044b9341e0c322b8fbec2db4a2cd8c2"
}
```
  
## REST API v. 0.1

Due to vast data structure change v. 0.1 was shut down.