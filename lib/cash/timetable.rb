module Cash
  
  class Timetable
        
    attr_reader :id, :params, :version, :activities
    
    # Constructor
    # 
    # 
    def initialize( data = {} )
      
      raise "Cannot initialize timetable without it's parameters" unless data["params"]
            
      @id = data["_id"] || data["id"] || Timetable.extract_id( data["params"] )
      @params = data["params"]
      @activities = data["activities"] || fetch_activities( params )
      @version = data["version"] || fetch_version( @activities )
      
    end
    
    def to_h
      {
        _id:        id,
        params:     params,
        version:    version,
        activities: activities
      }
    end
    
    def update 
      @activities = count_unit( fetch_activities( @params ) )
      @version = fetch_version( @activities )
      
      self  
    end
    
    def self.create( params )
      self.new( {
        "params"      => params,
        "activities"  => [],
        "version"     => nil,
      } )
    end
    
    def self.find( params, collection )
      if data = collection.find_one( { _id: self.extract_id( params ) } )
        self.new( data )
      end
    end
    
	  private
    
    # Fetch resource versions
    def fetch_version( activities )
      Digest::MD5.hexdigest( activities.to_s )
    end
    
    def fetch_activities( params )
      
      activity_ids = []
      
      if params["group_id"]
        activity_ids += Model::Participation.where( group_id: params["group_id"] ).pluck( :activity_id )        
      end
      
      if params["tutor_id"]
        activity_ids += Model::Assignment.where( tutor_id: params["tutor_id"] ).pluck( :activity_id )        
      end
      
      if params["place_id"]
        activity_ids += Model::Localization.where( place_id: params["place_id"] ).pluck( :activity_id )        
      end
      
      Model::Activity.order( :starts_at_timestamp )
                      .includes( :tutors, :groups, :places )
                      .find( activity_ids )
                      .map { |a| a.to_hash }
      
    end
    
    # TO TEST
    def count_unit( activities )
      
      counter = UnitCounter.new 
      
      activities.each do |a|
        counter.factor(unit_key(a)).tap do |c|
          a[:unit_ordinal_number] = c.hit
          a[:unit_total_count] = c.total
        end
      end
      
    end
    
    # Return string representation of params hash
    # E.q { "group_id" => [1, 45, 98] } will be converted to "g1g45g98" 
    #
    # @return String
    def self.extract_id( paramss )
      
      return paramss if paramss.is_a? String
      
      [].tap { |l| 
        paramss.each do |param, values|
          # Get first letter of a param key
          letter = param[0, 1]
          values.each { |v| l << "#{letter}#{v}" }
        end 
      }.join
      
    end
    
    private
    
    def unit_key( activity )
        ( [ activity[:name], activity[:category], activity[:state] ] + activity[:groups].map { |g| g[:id] } ).join( ',' )
    end
    
    def bind_patch( length )
      Array.new( length, "?" ).join( ', ' )
    end
  
  end
  
end