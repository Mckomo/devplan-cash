module Cash

	class Counter
	  
	  def initialize 
	    @counter = "0"
	  end
	  
	  def hit
	    @counter.replace( increment_string( @counter ) ); @counter.dup
	  end
	  
	  def total
	    @counter
	  end
	  
	  private 
	  
	  def increment_string( str )
	    ( str.to_i + 1 ).to_s 
	  end
	  
	end

	class UnitCounter
		
		def initialize
  		@counters = {}
    end
      
    def factor( unit_key )
			@counters[unit_key] ||= Counter.new	
		end
    
	end
	
end