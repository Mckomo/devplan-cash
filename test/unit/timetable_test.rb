require_relative '../support/helper'

require 'digest'

class TimetableTest < Test::Unit::TestCase
  
  context Cash::Timetable do
    
    should "extract string id from request parameters" do

    	params = {
    		"group_id" => [23, 533],
    		"tutor_id" => [9, 51],
    	}

    	assert_equal "g23g533t9t51", Cash::Timetable.extract_id( params )

    end

    should "create it's new instance" do

    	params = {
    		"group_id" => [23, 233],
    		"tutor_id" => [51],
    	}

        timetable = Cash::Timetable.create( params ).to_h

    	assert_equal "g23g233t51", timetable[:_id]
        assert_equal params, timetable[:params]
        assert timetable.keys.include?( :activities )
        assert timetable.keys.include?( :version )

    end

    should "fetch timetable from timetables collection" do

    end
  
  end
  
end