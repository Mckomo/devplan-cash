module Cash
  module Model
    
    # @author Mckomo
    class Participation < Cash::BaseModel
  
      belongs_to :group
      belongs_to :activity
      
    end
    
  end
end