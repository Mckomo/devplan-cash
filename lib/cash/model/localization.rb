module Cash
  module Model
    
    # @author Mckomo
    class Localization < Cash::BaseModel
  
      belongs_to :place
      belongs_to :activity
      
    end
    
  end
end