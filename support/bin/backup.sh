#!/usr/bin/env bash

TIMESTAMP=`date +%s`
HOME_DIR=~devplan

MONGO_BACKUP_FILE=$HOME_DIR/.backup/mongodb/$TIMESTAMP.nosql
MYSQL_BACKUP_FILE=$HOME_DIR/.backup/mysql/$TIMESTAMP.sql

# Mongodb and MySQL share same database name 
DATABASE="devplan_prod_master"

mongodump --db $DATABASE --out $MONGO_BACKUP_FILE
mysqldump $DATABASE > $MYSQL_BACKUP_FILE