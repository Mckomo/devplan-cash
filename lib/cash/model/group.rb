module Cash
  module Model
    
    # @author Mckomo
    class Group < Cash::BaseModel
      
      has_many :appointments 
      has_many :activities, :through => :appointments
      
      def to_hash 
        {
          id:   id,
          name: name,
          type: 1,
          type_name: "Grupa dziekańska",
        }
      end
      
    end
    
  end
end