require 'test/unit'
require 'shoulda/context'
require 'mocha/setup'
require 'sinatra/base'
require 'sinatra/namespace'
require 'rack/test'
require 'active_record'

require_relative '../../lib/cash'

ActiveRecord::Base.configurations = YAML::load( File.open( "config/database.yml" ) )
ActiveRecord::Base.establish_connection :test