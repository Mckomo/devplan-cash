require 'rubygems'
require 'bundler/setup'

require 'active_record'
require 'ar-multidb'
require 'sinatra/base'
require 'sinatra/namespace'
require 'mongo'
require 'rack/cache'
require 'redis-rack-cache'
require 'icalendar'

# Set Cash env
CASH_ENV = Sinatra::Base.settings.environment.to_sym unless defined? CASH_ENV

# Load Cash
require_relative 'lib/cash'

# Establish connection with the database
ActiveRecord::Base.configurations = YAML::load( File.open( "config/database.yml" ) )
ActiveRecord::Base.establish_connection CASH_ENV

# Enable cashing for production
if CASH_ENV == :production
	use Rack::Cache, metastore: 'redis://localhost:6379/0/metastore', entitystore: 'redis://localhost:6379/0/entitystore'
end

# Init API with MongoDb storage system
api = Cash::API.new.tap do |a|
  a.helpers.timetables = Mongo::MongoClient.new.db( ActiveRecord::Base.connection_config[:database] ).collection('timetables')
end

# Run the API!
run api