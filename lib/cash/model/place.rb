module Cash
  module Model
    
    # @author Mckomo
    class Place < Cash::BaseModel
      
      has_many :activities
      
      def to_hash 
        {
          id:             id,
          name: short_location,
          full_name:  full_location,
          regular:        true
        }
      end
      
    end
    
  end
end