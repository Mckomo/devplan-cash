# Cash
# 
# Gabriel Cash was always "mouth" in Tango & Cash partnership. 
# So it is in this case. Cash delivers content through 
# a RESTful fashioned API based on Sinatra Framework.

module Cash
  
  VERSION = "0.2.0"
  
  # This Web API is fundamental element of Cash
  # It provides access to all Tango resources
  #
  # @see    [api_documentation]
  # @author Mckomo
  class API < Sinatra::Application
    
    attr_accessor :timetables
    
    # Register Sinatra::Namespace to implement URL groups
    register  Sinatra::Namespace
    helpers   Cash::SinatraHelper

    # Respond only in JSON format
    before do
      content_type :json, :charset => 'UTF-8'
      parse_input if request.post?
      enable_cors
    end

    after do
      if content_type.include?('application/json')
        body_to_json
      end
    end
    
    not_found do
      error( 404, "Page not found" )
    end
        
    # Create timetable with requested resources ( as params )
    post "/api/timetables" do
      
      # Get filtered and sorted params
      timetable_params = Parameters.new( param_whitelist, params )
      
      # If no correct params, respond with "Bad request" status  
      if timetable_params.empty?
        halt error( 400, "Bad Request. No supported parameter was provided. Supported parameters are #{param_whitelist.join(', ')}" )
      end
      
      # Try to fetch timetable with same params
      timetable = Timetable.find( timetable_params, @timetables )
      
      # Skip if timetable already exists
      unless timetable
        timetable = Timetable.create( timetable_params )
        @timetables.insert( timetable.to_h )
        # Change response status to "Resource created"
        response.status = 201
        logger.info "Registered timetable with params #{timetable_params}"
      end
                              
      # Return requested timetable credentials
      {
        _id: timetable.id,
        access_url: "#{request_url}/#{timetable.id}",
        params: timetable.params
      }
              
    end
    
    # Return timetable with given id
    get "/api/timetables/:id.?:format?/?:scope?" do |id, format, scope|
              
      # Fetch timetable from data base        
      timetable = Timetable.find(id, @timetables)
      
      # If there is no such a timetable respond with "Bad request" status 
      if ! timetable          
        halt error( 404, "Not Found. Timetable with given id was not found." )
      end
      
      cache_control :public, max_age: 3600
      
      # Get version of timetable before update
      pre_version = timetable.version.dup

      # Update timetable
      timetable.update

      # Save changes if any
      @timetables.save( timetable.to_h ) unless timetable.version == pre_version

      # Respond in iCalendar format
      if format.eql?('ics')

        content_type :ics

        cal = Icalendar::Calendar.new
        cal.append_custom_property('X-WR-CALNAME', 'devPlan')

        cal.timezone do |t|

            t.tzid = "Europe/Warsaw"

            t.daylight do |d|
              d.tzoffsetfrom = "+0100"
              d.tzoffsetto   = "+0200"
              d.tzname       = "CEST"
              d.dtstart      = "19700329T020000"
              d.rrule        = "FREQ=YEARLY;BYMONTH=3;BYDAY=-1SU"
            end

            t.standard do |s|
              s.tzoffsetfrom = "+0200"
              s.tzoffsetto   = "+0100"
              s.tzname       = "CET"
              s.dtstart      = "19701025T030000"
              s.rrule        = "FREQ=YEARLY;BYMONTH=10;BYDAY=-1SU"
            end

        end
        
        timetable.activities.each do |a|
          cal.event do |e|
            e.dtstart     = Time.at(a[:starts_at_timestamp])
            e.dtend       = Time.at(a[:ends_at_timestamp])
            e.location    = a[:places].map { |p| p[:full_name] }.join(', ')
            e.summary     = "#{a[:category_name]} z #{a[:name]}"
          end
        end

        return cal.to_ical

      end

      if scope == "version"
        {
          _id: timetable.id,
          version: timetable.version
        }
      else
        timetable.to_h
      end
      
    end
    
    # Get list of all tutors
    get "/api/tutors" do
      cache_control :public, max_age: 86400
      Model::Tutor.all.to_hash
    end
    
    # Get list of all groups
    get "/api/groups" do
      cache_control :public, max_age: 86400
      Model::Group.all.to_hash
    end
    
    # Get list of all rooms
    get "/api/places" do
      cache_control :public, max_age: 86400
      Model::Place.all.to_hash
    end
    
    # Change currently used database (after Tango interval)
    post "/api/settings" do
      
      # Allow to toggle shards only from localhost
      if ! request.ip.eql?( "127.0.0.1" )
        return error( 403, "Not authorized" )
      end
      
      # Get database from POST params
      begin
        Multidb.use( params[:database] )
        response.status = 201
      rescue
        params[:database] = :default
        Multidb.use( params[:database] )
      end
              
      { database: params[:database]  }
      
    end
        
    # Prevent 404 responses for favicon request
    get "/favicon.ico" do
      nil
    end
    
    private
    
    # Whitelist of timetable params
    # 
    # @return   [Array]
    def param_whitelist
      [ "group_id", "tutor_id", "place_id" ]
    end
    
  end
  
end